import React,{Component} from 'react'
import axios from 'axios';
//import * as _ from 'lodash';
import SearchBar from './components/SearchBar';
import './styles/main.css';

class App extends Component{
  constructor(){
    super();
     this.state={
      data:[],
    search:''
    }
    this.onSearchSubmit=this.onSearchSubmit.bind(this);
  }
  //run fucntion on every first render
    componentDidMount(){
        this.onSearchSubmit()
    }
    
    async onSearchSubmit(){
       const URL = 'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json';
       const response= await axios.get(URL);
        //set the state value 
        this.setState({data:response.data});
        // console.log(this.state.data);
  
      }
       filter(event){
        // console.log(term);
        this.setState({search:event.target.value.substr(0,20)});
        console.log(this.state.search);
       }
      getId(id){
        console.log(`ID is equals to: ${id}`)
      }

      
  render(){
    const filteredUsers = this.state.data.filter(
      (user)=>{
        //implementing case sensitive 
        return user.first_name.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1;
      }
    ); 
    const renderList =filteredUsers.map(item=>{
     // const sorted=_.orderBy(this.state.data,r=>r.first_name,['asc']);
      
      return(
         <div 
            className="data"
            onClick={()=>this.getId(item.id)}
            key={item.id}>
              <img className="avatarImage" alt='Avatar:' src={item.avatar}></img>
              <div>
                <h3>{item.first_name}{' '}{item.last_name}</h3>
                <h5>{item.email}</h5>
              </div>   
      </div>
      )
    });
   
    return(
        <>
        <header className="header">
           <h1>Contacts</h1>
        </header>
      
        <div className="ui transparent left icon input searchBox">
                         <input 
                            type="text" 
                            placeholder="Search for friends..." 
                            value={this.state.term}  
                            onChange={e=>this.setState({search:e.target.value})} />
                        <i className="search icon"></i>
                    </div> 
       {renderList}
        </>
    );
  }
}
export default App;